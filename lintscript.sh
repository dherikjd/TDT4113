for f in $(find . -type f -name "*linterenabled.py")
do
    pylint $f --output-format=text --exit-zero | tee -a pylint.txt
done
(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' pylint.txt) > score.txt
avg=$(python -c "x = open('score.txt'); lines=x.read().strip().split('\n'); print(round(sum(float(i) for i in lines)/len(lines), 2))")
anybadge --value=$avg --file=pylint.svg pylint